﻿(function () {

    var app = angular.module("loadingViewer");

    var loadingCtrl = function ($scope,$interval,Imagesobj, ModalService) {
        this.Images = Imagesobj;

        var decrementCountdown = function () {
            $scope.image = Imagesobj[$scope.countdown];
            $scope.countdown += 1;

            if ($scope.countdown > 5) {
                $interval.cancel(countdownInterval);
            }
        }

        var countdownInterval = null;
        var startCountdown = function (countdown) {
            countdownInterval = $interval(decrementCountdown, 1000, $scope.countdown)
        }

        
        $scope.countdown = 0;
        startCountdown($scope.countdown);


    };
    app.controller("loadingCtrl", loadingCtrl)
}());

