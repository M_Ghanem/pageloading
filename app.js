﻿(function () {

    var app = angular.module("loadingViewer", ['ui.router', 'angularModalService']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/loading');

        $stateProvider
            .state("loading", {
                url: "/loading",
                templateUrl: "loading.html",
                controller: "loadingCtrl as vm",
                resolve: {
                    Imagesobj: function (loading) {
                        return loading.getImages();
                    }
                }
            });
    });


}());
