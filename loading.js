﻿(function () {
    var picList = [
        { "src":"img/01.jpg" },
        { "src":"img/02.jpg" },
        { "src":"img/03.jpg" },
        { "src":"img/04.jpg" },
        { "src":"img/05.jpg" }]

    var loading = function () {

        var getImages = function () {
            return picList;
        };

        return {
            getImages: getImages
        };
    };

    var module = angular.module("loadingViewer");
    module.factory("loading", loading);
}());